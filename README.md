# Build Directions:
`go get`

`go build -o brute main.go`

or

`go install`

```
Usage of ./brute:
  -hostname string
    	hostname to test (default "example.com")
  -list string
    	file with subdomains separated by blank lines (default "subdomains.txt")
  -log-level string
    	potential values: debug, info, warn, error (default "info")
  -out string
    	Log output to a file with hostname and IPv4 address. (default "none")
  -proc int
    	number of processes to run testing subdomains. 40+ may run into i/o issues (default 10)
```
