package main

import (
	"bufio"
	"flag"
	"gitlab.com/greensilex/zap_log_wrapper"
	"net"
	"os"
	"regexp"
	"runtime"
	"strings"
	"sync"
	"time"
)

var prelogger, _ = greensilexZapWrapper.Log("info")
var logger = prelogger.Sugar()
var fileSubs, hostname, logFile string
var procNum = 10

var wg sync.WaitGroup

func init() {

	// set file to lookup hostnames
	var file = flag.String("list", "subdomains.txt", "file with subdomains separated by blank lines")

	// set hostname to test
	var host = flag.String("hostname", "example.com", "hostname to test")

	// allow changing log level
	var level = flag.String("log-level", "info", "potential values: debug, info, warn, error")

	// number of processes
	var proc = flag.Int("proc", 10, "number of processes to run testing subdomains. 40+ may run into i/o issues")

	// write output to a file
	var outFile = flag.String("out", "none", "Log output to a file with hostname and IPv4 address.")

	flag.Parse()
	logLevel := *level
	fileSubs = *file
	hostname = *host
	procNum = *proc
	logFile = *outFile

	// Logging setup
	prelogger, _ = greensilexZapWrapper.Log(logLevel)
	logger = prelogger.Sugar()
	logger.Debug("loglevel: ", logLevel)
}

func main() {
	runtime.GOMAXPROCS(2) // set the number of CPUs to use

	subdomainChan := make(chan string)

	// If we passed a log file, let's make sure that it exists and we can write to it.
	if logFile != "none" {
		logger.Debug("Checking that log file existsg")
		fileExists(logFile)
	}

	// We want to check that the main domain is valid; otherwise, just quit now
	hostDns, err := net.LookupHost(hostname)
	if err != nil {
		logger.Info("no hostname for: ", hostname, " Exiting Program")
		return
	} else {
		mainDomain := strings.Join([]string{hostname, " ", hostDns[0]}, "")
		logger.Info(mainDomain)
		writeToFile(mainDomain)
	}

	// read the file with subdomains
	go subdomainRead(subdomainChan)

	// launch the number of go routines per the proc flag
	// the dns lookup takes the most time so we can run a lot of these
	for i := 0; i < procNum; i++ {
		go resolveDns(subdomainChan)
	}

	// It's possible to bypass the wait group before any wait group counter
	// is even triggered; wait 1 second to make sure this didn't run too fast
	time.Sleep(1 * time.Second)

	wg.Wait() // wait until done
}

func subdomainRead(subdomainOut chan string) {
	wg.Add(1)

	logger.Debug("fileSubs = ", fileSubs)

	// Open the file
	file, err := os.Open(fileSubs)
	if err != nil {
		logger.Info(err)
	}
	defer file.Close()

	// Read through the file line by line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		logger.Debug("scanned text: ", scanner.Text())
		subdomainOut <- scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		logger.Info(err)
	}

	// close the channel when we are done reading from the file
	close(subdomainOut)
	wg.Done()

}

func resolveDns(subdomainIn chan string) {
	wg.Add(1)

	// listening on the channel and joining the subdomain and domain
	for sub := range subdomainIn {
		subAndHost := strings.Join([]string{sub, ".", hostname}, "")
		logger.Debug("hostname: ", subAndHost)

		hostDns, err := net.LookupHost(subAndHost)

		// handle the errors
		// we get two types, 1 the subdomain doesn't exist so just log to debug
		if err != nil {
			logger.Debug(err)
			matched, _ := regexp.MatchString("i/o timeout", err.Error())

			// the error we really want to handle is an i/o timeout error
			// this means your internet is having trouble or you have too many
			// network threads open and your kernel can't open that many
			// connections; here we have failed to actually check the subdomain
			// so we'll try again until things are fixed.
			for matched {
				logger.Info("the number of processes is set too high.", err)
				logger.Info("retrying ", subAndHost)
				hostDns, err = net.LookupHost(subAndHost)
				matched, _ = regexp.MatchString("i/o timeout", err.Error())
			}

		} else {
			hostAndIp := strings.Join([]string{subAndHost, " ", hostDns[0]}, "")
			//logger.Info(subAndHost, ": ", hostDns[0])
			logger.Info(hostAndIp)
			writeToFile(hostAndIp)
		}
	}

	wg.Done()
}

func writeToFile(hostAndIp string) {

	logger.Debug("writeToFile function")
	// if we passed a logfile, let's write to it.
	if logFile != "none" {
		logger.Debug("Writing to log file")
		file, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err != nil {
			logger.Error("Cannot create file", err)
		}
		defer file.Close()

		// add a new line to string for easier reading
		hostAndIp = strings.Join([]string{hostAndIp, "\n"}, "")
		_, err = file.WriteString(hostAndIp)
		if err != nil {
			logger.Error(err)
		}
	}

	return
}

func fileExists(logFile string) {

	logger.Debug("fileExists function")
	// let's check that the file exists and create it if it doesn't
	if _, err := os.Stat(logFile); os.IsNotExist(err) {
		logger.Info(logFile, ": file doesn't exist")

		// let's create the file
		logger.Debug("Trying to create the file", logFile)
		f, fileErr := os.Create(logFile)
		defer f.Close()
		if fileErr != nil {
			logger.Error("Failed to create log file")
			panic(fileErr.Error())
		}
	}
	return
}
